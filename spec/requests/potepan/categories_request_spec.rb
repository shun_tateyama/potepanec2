RSpec.describe Potepan::CategoriesController, type: :request do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it '正常なレスポンスである' do
      expect(response).to be_successful
    end

    it 'リクエストが成功する' do
      expect(response.status).to eq 200
    end

    it '商品一覧画面にレンダリングされる' do
      expect(response).to render_template(:show)
    end

    it 'カテゴリー名が取得されている' do
      expect(response.body).to include taxon.name
    end
  end
end
