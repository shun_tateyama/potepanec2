RSpec.describe Potepan::ProductsController, type: :request do
  describe 'GET #show' do
    let(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it '正常なレスポンスであること' do
      expect(response).to be_successful
    end

    it 'リクエストが成功すること' do
      expect(response.status).to eq 200
    end

    it '商品名が取得されていること' do
      expect(response.body).to include product.name
    end

    it '商品価格が取得されていること' do
      expect(response.body).to include product.display_price.to_s
    end

    it '商品説明が取得されていること' do
      expect(response.body).to include product.description
    end
  end
end
