RSpec.feature "Products", type: :feature do
  given(:taxon) { create(:taxon) }
  given(:taxon2) { create(:taxon, taxonomy: taxonomy) }
  given(:taxonomy) { create(:taxonomy) }
  given(:product) { create(:product, taxons: [taxon]) }
  given!(:related_products)  { create_list(:product, 5, taxons: [taxon]) }
  given!(:related_products2) { create(:product, taxons: [taxon2]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "「一覧ページへ戻る」リンクをクリックして、/potepan/categories/(taxon.id)に遷移できる" do
    click_on '一覧ページへ戻る'
    expect(page).to have_current_path potepan_category_path(taxon.id)
  end

  scenario "商品詳細ページの画面表示の確認" do
    within(".singleProduct") do
      expect(page).to have_content(product.name, count: 1)
      expect(page).to have_content(product.display_price, count: 1)
      expect(page).to have_content(product.description, count: 1)
      expect(page).to have_link '一覧ページへ戻る'
    end
    expect(page).to have_content(product.name, count: 3)
    expect(page).to have_link('Home', count: 2)
    expect(page).to have_link 'Shop'
  end

  scenario "関連商品をクリックして、/potepan/product/(related_product.id)に遷移できる" do
    within(".productsContent") do
      click_on related_products[0].name
      expect(page).to have_current_path potepan_product_path(related_products[0].id)
    end
  end

  scenario "関連商品が表示される" do
    within(".productsContent") do
      expect(page).to have_link related_products[0].name
      expect(page).to have_link related_products[0].display_price
    end
  end

  scenario "関連しない商品は表示されない" do
    within(".productsContent") do
      expect(page).not_to have_link related_products2.name
    end
  end

  scenario "関連商品が4つ表示される" do
    within(".productsContent") do
      expect(page).to have_selector ".productBox", count: 4
    end
  end
end
