RSpec.feature "Categories", type: :feature do
  given(:taxonomy) { create(:taxonomy) }
  given(:taxon) { create(:taxon, name: "Bags", taxonomy_id: 1, parent_id: taxonomy.root.id) }
  given!(:product) { create(:product, name: "RUBY ON RAILS TOTE", id: 1, taxons: [taxon]) }
  given(:other_taxon) { create(:taxon, name: "Mugs", taxonomy_id: 2, parent_id: taxonomy.root.id) }
  given!(:other_product) { create(:product, name: "RUBY ON RAILS BAG", taxons: [other_taxon]) }
  before do
    visit potepan_category_path(taxon.id)
  end

  scenario "カテゴリーページが表示される" do
    within(".pageHeader") do
      expect(page).to have_content(taxon.name, count: 2)
      expect(page).to have_link "Home"
      expect(page).to have_link "shop"
    end
    within(".sideBar") do
      expect(page).to have_content "商品カテゴリー"
      expect(page).to have_content "色から探す"
      expect(page).to have_content "サイズから探す"
      expect(page).to have_link taxonomy.name
      expect(page).to have_link taxon.name
      expect(page).to have_link taxon.products.count
    end
    within(".mainBar") do
      within("div.productBox") do
        expect(page).to have_link product.name
        expect(page).to have_link product.display_price
      end
    end
  end

  scenario "商品カテゴリーが表示される" do
    within(".sideBar") do
      expect(page).to have_content taxonomy.name
      click_on taxonomy.name
      expect(page).to have_content taxon.name
      click_on taxon.name
      expect(page).to have_current_path potepan_category_path(taxon.id)
    end
  end

  scenario "表示されているproductをクリックしてページ遷移できる" do
    within(".mainBar") do
      expect(page).to have_content product.name
      click_on product.name
      expect(page).to have_current_path potepan_product_path(product.id)
    end
  end

  scenario "taxonに紐づかないproductが表示されていない" do
    within(".mainBar") do
      expect(page).not_to have_content other_product.name
    end
  end

  scenario "HOMEをクリックしてページ遷移できる" do
    click_link 'Home', href: potepan_index_path, match: :first
    expect(page).to have_current_path potepan_index_path
  end
end
