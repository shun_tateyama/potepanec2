RSpec.describe ApplicationHelper do
  describe 'full_title(page_title)' do
    let(:base_title) { ApplicationHelper::BASE_TITLE }

    context 'page_titleが空ではない場合' do
      it 'RUBY ON RAILS TOTE - #{base_title}を得る' do
        expect(full_title("RUBY ON RAILS TOTE")).to eq "RUBY ON RAILS TOTE - #{base_title}"
      end
    end

    context 'page_titleが空である場合' do
      it 'base_titleを得る' do
        expect(full_title("")).to eq base_title
      end
    end

    context 'page_titleがnilである場合' do
      it 'base_titleを得る' do
        expect(full_title(nil)).to eq base_title
      end
    end
  end
end
