class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_MAX_COUNT = 4
  def show
    @product = Spree::Product.find(params[:id])
    @taxon = @product.taxons.first
    @related_products = @product.related_products.display_limit(RELATED_PRODUCTS_MAX_COUNT)
  end
end
