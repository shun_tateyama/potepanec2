Spree::Product.module_eval do
  def related_products
    Spree::Product.in_taxons(taxons).where.not(id: id).distinct.order(:id).includes(master: [:images, :default_price])
  end
  scope :display_limit, ->(display_limit) { limit(display_limit) }
end
